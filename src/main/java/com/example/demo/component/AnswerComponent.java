package com.example.demo.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.example.demo.dto.AnswerDto;
import com.example.demo.exception.ApiException;
import com.example.demo.exception.ApiExceptionDomain.ExceptionType;

@Component
public class AnswerComponent {
	
	@Autowired
	RestTemplate restTemplate;
	
	@Value("http://localhost:8003/api/answer")
	String answerURL;

	public AnswerDto getAnswer(Long id) throws ApiException {
		try{
			String finalUrl = answerURL + '/' + id;
			return  restTemplate.getForEntity(finalUrl, AnswerDto.class).getBody();
			
		} catch(Exception e) {
			throw new ApiException(AnswerDto.class.getSimpleName(), id.toString(),
					ExceptionType.NOT_FOUND);
		}
		
	}
}
