package com.example.demo.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.ResultDto;
import com.example.demo.dto.VoteDto;
import com.example.demo.dto.VoteFilterDto;
import com.example.demo.dto.VotePersistDto;
import com.example.demo.exception.ApiException;
import com.example.demo.service.VoteService;

@RestController
@RequestMapping("/api/vote")
class VoteController {

	private final Logger log = LoggerFactory.getLogger(VoteController.class);

	@Autowired
	private VoteService service;

	@GetMapping
	Collection<VoteDto> votes(VoteFilterDto filter) {
		return service.find(filter);
	}

	@GetMapping("/{id}")
	ResponseEntity<?> vote(@PathVariable Long id) {
		
		Optional<VoteDto> result = service.find(id);
		return result.map(response -> ResponseEntity.ok().body(response)).orElse(ResponseEntity.notFound().build());
	}

	@PostMapping
	ResponseEntity<ResultDto> create(
			@Valid @RequestBody VotePersistDto dto
			) throws URISyntaxException, ApiException {

		log.info("Request to create vote: ", dto);
		Long idVote = service.create(dto);
		return ResponseEntity.created(new URI("/api/vote/" + idVote))
				.body(new ResultDto(idVote, HttpStatus.CREATED));
	}

	@PutMapping("/{id}")
	ResponseEntity<ResultDto> update(
			@PathVariable Long id, 
			@Valid @RequestBody VotePersistDto dto
			) throws ApiException {
		
		log.info("Request to update vote: ", dto);
		service.update(dto, id);
		return ResponseEntity.ok()
				.body(new ResultDto(id, HttpStatus.OK));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> remove(@PathVariable Long id) throws ApiException {
		
		log.info("Request to delete vote: ", id);
		service.remove(id);
		return ResponseEntity.ok().body("Vote removed: " + id);
	}

}