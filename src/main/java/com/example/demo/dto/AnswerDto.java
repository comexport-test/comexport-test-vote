package com.example.demo.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class AnswerDto {
	
	Long idAnswer;
	
	@NotNull
	Long idUser;
	
	@NotNull
	Long idQuestion;
	
	@NotNull
	String comment;
}
