package com.example.demo.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class VoteDto {
	
	@NotNull
	Long id;
	
	@NotNull
	Long idAnswer;
	
	@NotNull
	Long idUser;
	
	@NotNull
	Long score;
	
	LocalDateTime createdAt;
	LocalDateTime updatedAt;
}
