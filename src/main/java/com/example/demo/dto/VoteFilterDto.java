package com.example.demo.dto;

import lombok.Data;

@Data
public class VoteFilterDto {
	Integer page;
	Integer size;
	Long idAnswer;
	Long idUser;
}
