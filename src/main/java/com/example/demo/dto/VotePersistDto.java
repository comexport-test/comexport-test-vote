package com.example.demo.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class VotePersistDto {
	
	@NotNull
	Long idAnswer;
	
	@NotNull
	Long idUser;
	
	@NotNull
	Long score;
	
}
