package com.example.demo.exception;

import com.example.demo.exception.ApiExceptionDomain.ExceptionType;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class ApiExceptionDto {

	private String entidade;
	
	private String id;
	
	private ExceptionType tipo;

	private String mensagem;
	
}
