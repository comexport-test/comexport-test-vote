package com.example.demo.model.mapped;

import javax.persistence.Id;
import javax.persistence.IdClass;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@IdClass(com.example.demo.model.mapped.VotePK.class)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VotePK {
	
	@Id
	Long idQuestion;
	
	@Id
	Long idAnswer;
	
	@Id
	Long idUser;
	
}
