package com.example.demo.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.component.AnswerComponent;
import com.example.demo.component.UserComponent;
import com.example.demo.dto.VoteDto;
import com.example.demo.dto.VoteFilterDto;
import com.example.demo.dto.VotePersistDto;
import com.example.demo.exception.ApiException;
import com.example.demo.exception.ApiExceptionDomain.ExceptionType;
import com.example.demo.model.Vote;
import com.example.demo.repository.VoteRepository;

@Service
public class VoteService {

	@Autowired
	private VoteRepository voteRepository;
	
	@Autowired
	private UserComponent userComponent;
	
	@Autowired
	private AnswerComponent answerComponent;
	
	private ModelMapper modelMapper;

	@Autowired
	public VoteService(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

	public List<VoteDto> find(VoteFilterDto filter) {
		Vote voteExample = new Vote();
		voteExample.setIdUser(filter.getIdUser());
		voteExample.setIdAnswer(filter.getIdAnswer());

		List<VoteDto> result = new ArrayList<VoteDto>();

		int page = filter.getPage() != null ? filter.getPage() : 0;
		int size = filter.getSize() != null && filter.getSize() < 1000 ? filter.getSize() : 1000;
		Pageable basePage = PageRequest.of(page, size);
		
		ExampleMatcher customExampleMatcher = ExampleMatcher.matchingAll()
				.withMatcher("idAnswer", ExampleMatcher.GenericPropertyMatchers.exact())
				.withMatcher("idUser", ExampleMatcher.GenericPropertyMatchers.exact());

		Example<Vote> example = Example.of(voteExample, customExampleMatcher);
		Page<Vote> queryResult = voteRepository.findAll(example, basePage); 
		queryResult.map(vote -> result.add(convertToDto(vote)));
		return result;
	}
	
	public Optional<VoteDto> find(Long id) {
		return voteRepository.findById(id).map(vote -> convertToDto(vote));
	}

	public Long create(VotePersistDto dto) throws ApiException {
		
		userComponent.getUser(dto.getIdUser());
		answerComponent.getAnswer(dto.getIdAnswer());
		
		Vote vote = new Vote();
		vote.setIdUser(dto.getIdUser());
		vote.setIdAnswer(dto.getIdAnswer());
		vote.setScore(dto.getScore());

		LocalDateTime now = LocalDateTime.now();
		vote.setUpdatedAt(now);
		vote.setCreatedAt(now);
		
		voteRepository.save(vote);
		return vote.getId();
	}

	public void update(VotePersistDto dto, Long id) throws ApiException {
		
		Vote original = voteRepository.findById(id)
				.orElseThrow(() -> new ApiException(Vote.class.getSimpleName(), 
						id.toString(), ExceptionType.NOT_FOUND));
		
		userComponent.getUser(dto.getIdUser());
		answerComponent.getAnswer(dto.getIdAnswer());
		
		original.setIdUser(dto.getIdUser());
		original.setIdAnswer(dto.getIdAnswer());
		original.setScore(dto.getScore());
		
		original.setUpdatedAt(LocalDateTime.now());

		voteRepository.save(original);
	}
	
	public void remove(@Valid Long id) throws ApiException {
		
		//TODO delete answers
		
		Vote original = voteRepository.findById(id)
				.orElseThrow(() -> new ApiException(Vote.class.getSimpleName(), id.toString(), ExceptionType.NOT_FOUND));
		
		voteRepository.delete(original);
	}
	
	private VoteDto convertToDto(Vote vote) {
		VoteDto dto = modelMapper.map(vote, VoteDto.class);
		return dto;
	}

}
